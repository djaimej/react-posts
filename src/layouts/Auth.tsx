import React from 'react';
import { useNavigate } from 'react-router';
import axios from "axios";
import { UserResponse } from '../interfaces/General';

export default function Auth(): JSX.Element {
    const navigate = useNavigate();

    const instance = axios.create();
    
    const Login = async () => {
      await instance.post<UserResponse>("https://reqres.in/api/login",{
        "email": "eve.holt@reqres.in",
        "password": "cityslicka"
      }).then(response => {
        console.log(response.data);
        navigate('/posts');
      }).catch(err => {
        console.log(err);
      })
    }

  return (
    <main className="Auth">
      <section className="Head">
        <svg fill="#fff" id="icons" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
          <path d="M487.83,319.44,295.63,36.88a48,48,0,0,0-79.26,0L24.17,319.44A47.1,47.1,0,0,0,41.1,387.57L233.3,490.32a48.05,48.05,0,0,0,45.4,0L470.9,387.57a47.1,47.1,0,0,0,16.93-68.13Zm-431.26,41a16.12,16.12,0,0,1-8-10.38,16.8,16.8,0,0,1,2.37-13.62L232.66,69.26c2.18-3.21,7.34-1.72,7.34,2.13v374c0,5.9-6.54,9.63-11.87,6.78Z"/>
        </svg>
        <p>Welcome back!</p>
      </section>
      <form className="Form">
        <input autoComplete="" className="text" placeholder="Email address" type="email" id="email" />
        <input className="text" placeholder="Password" type="password" id="pass" />
        <input onClick={Login} className="button" type="button" value="Login" />
      </form>
    </main>
  );
}
  