import React from 'react';
import { Outlet } from 'react-router';

export default function Main(): JSX.Element {
  
    return (
      <div>
      <Outlet />
      </div>
    );
  }