import { Post } from "../interfaces/General";
import React, { useState } from "react";
import axios from "axios";
import { Category } from "../components/Category/Category";

export const Posts = () => {
    const [posts, setPosts] = useState<Post[]>([]);

    const instance = axios.create();
    React.useEffect(() => {
        instance.get<Post[]>( "https://jsonplaceholder.typicode.com/posts")
        .then(response => {
            setPosts(response.data);
        }).catch(err => {
            console.log(err);
        })
    })
    
    return <ul>
        {
            posts.map( post => <li 
                key={post.id.toString()}><Category {...post} ></Category></li> )
        }
    </ul>;
};
