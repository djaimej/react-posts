import React from 'react';
import { useNavigate } from 'react-router';

export const Dashboard = () => {
  const navigate = useNavigate();
  
  React.useEffect(() => {
    navigate('auth');
  })
  return <div>Dashboard</div>;
};
