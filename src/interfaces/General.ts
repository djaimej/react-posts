
export interface UserResponse {
    token: string;
}

export interface Post {
    userId: number;
    id: number;
    title: string;
    body: string;
}
