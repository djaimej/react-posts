
export type Severity = 'error' | 'warning' | 'info' | 'success';

export interface ISnackAlert {
    alertSeverity: Severity;
    alertMessage: string;
    openAlert: boolean
}
