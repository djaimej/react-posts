import React from "react";
import { Post } from '../../interfaces/General';

import "./Category.css";



interface State {
  active: boolean;
}

export class Category extends React.Component<Post, State> {

  static defaultProps: Post = {
    body: "",
    id: 0,
    title: "",
    userId: 0
  }

  state: Readonly<State> = {
      active: false
  }

  render() {
      return (
        <section className="Post">
            <h3>{this.props.title}</h3>
            <p>{this.props.body}</p>
        </section>
      )
  }
}