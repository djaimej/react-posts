import React from 'react';

import { QueryClient, QueryClientProvider } from 'react-query';

import './App.css';
import { GlobalProvider } from './context/GlobalContext';
import AppRouter from './routes/AppRouter';

const queryClient = new QueryClient();

function App() {
  return (
    <GlobalProvider>
        <QueryClientProvider client={queryClient}>
          <AppRouter />
        </QueryClientProvider>
    </GlobalProvider>
  );
}

export default App;
