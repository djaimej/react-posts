import { UserResponse } from '../interfaces/General';
import React from 'react';

const GlobalContext = React.createContext<{
    userData?: UserResponse;
    setUserData?: React.Dispatch<React.SetStateAction<UserResponse>>;
}
>({});

function GlobalProvider(props: { children: JSX.Element }): JSX.Element {
    const [userData, setUserData] = React.useState<UserResponse>({
        token: ''
    });
    
    return (
        <GlobalContext.Provider value={
            {
                userData,
                setUserData
            }
        }>
            {props.children}
        </GlobalContext.Provider>
    );
}

export { GlobalContext, GlobalProvider };
