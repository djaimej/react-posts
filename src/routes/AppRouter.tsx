import React from 'react';
/* PAGES */
import { NotFound } from '../pages/NotFound';
import { Dashboard } from '../pages/DashBoard';
import { Posts } from '../pages/Posts';

/* PAGES */

/* AUTH */
import { Route, Routes } from 'react-router';
import Main from '../layouts/Main';
import Auth from '../layouts/Auth';
/* AUTH */

export default function AppRouter(): JSX.Element {
  return (
    <div>
      <Routes>        
        <Route path="*" element={<NotFound />} />
        <Route path='/' element={<Main />}>
          <Route path='/' element={<Dashboard />} />
          <Route path='/posts' element={<Posts />} />
        </Route>
        <Route path="auth" element={<Auth />} />
      </Routes>
    </div>
  );
}

